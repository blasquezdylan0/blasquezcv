<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("./asset/include/cssLinker.php") ?>

    <title>Blasquez Dylan - Portfolio</title>
</head>

<body>
    <?php
    try {
        $bdd = new PDO('mysql:host=db5002165775.hosting-data.io;dbname=dbs1752669;charset=utf8', 'dbu98313', 'X0mL25*@p)=-1033');
        //$bdd = new PDO('mysql:host=127.0.0.1;dbname=dbs1752669;charset=utf8', 'root', '');
    } catch (Exception $e) {
        echo 'Erreur : ' . $e->getMessage() . '<br />';
        echo 'N° : ' . $e->getCode();
    }
    ?>
    <?php include("./asset/include/navbar.php") ?>

    <div class="background">

    </div>

    <section id="home">

    </section>

    <div class="container presentation-card mt-5 p-5">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="./asset/image/profilPro-min.jpg" class="w-100" alt="Photo perso">
            </div>
            <div class="col-sm-12 col-md-8 ">
                <div class="d-flex flex-column">
                    <h1><strong>Blasquez</strong> Dylan</h1>
                    <span class="separator"></span>
                    <p class="mt-1 poste">Developpeur Web</p>
                </div>
                <div class="presentation-text">
                    Développeur marseillais passionné par le Web, je veux faire de cette passion mon métier. Ce site présente les différents projets que j'ai pu mener ainsi que mon expérience dans plusieurs technologies avec lesquelles j'ai travaillé. <br> <br> Diplômé de <a href="https://www.institut-g4.fr/" target="_blank""> l'institut G4</a>, j'ai pu développer des compétences techniques et de management. Cette bivalence me permet de comprendre les différents aspects des projets sur lesquels je travaille.  
                </div>
            </div>
        </div>
    </div>

    <section id=" skills">

    </section>

    <section id="projects">
        <div class="container">
            <div class="card-container">
                <?php
                    $resultats = $bdd->query("SELECT * FROM project");
                        $resultats->setFetchMode(PDO::FETCH_OBJ);
                        while ($ligne = $resultats->fetch()) { ?>
                            <div class="card p-2 m-2" style="width: 18rem;">
                            <div class="imageCardContainer">
                            <img class="card-img-top" src="<?php echo $ligne->imagePath ?>" alt="<?php $ligne->imageCapt ?>">
                         </div>
                         <div class="card-body">
                             <h5 class="card-title"><?php echo $ligne->name ?></h5>
                             <p class="card-text"><?php echo $ligne->description ?></p>
                         </div>
                     </div>
                 <?php } ?>
            </div>
        </div>
        <div class="see-more-button d-flex justify-content-center mt-3">
            <a href="projectView.php"><button class="btn btn-primary">Voir plus de projets!</button></a>
        </div>
    </section>





    <?php include("./asset/include/footer.php") ?>
    <?php include("./asset/include/jsLinker.php") ?>
</body>

</html>