<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="logo" src="asset/image/logoBDLong-White.png" alt="Logo black de BLASQUEZ Dylan - Portfolio">
            </div>
            <div class="col-md-4">
                <h2>Socials</h2>
                <span class="separator-contact"></span>
                <div class="socials d-flex  mt-1">
                    <ul class="link-font d-flex justify-content-between w-50">
                        <li><a href="https://www.facebook.com/dylan.blasquez.370"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://www.instagram.com/blasquezdylan/"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/dylan-blasquez-64b033163/"><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <h2>Contact</h2>
                <span class="separator-contact"></span>
                <ul class="contact-list">
                    <li><i class="far fa-envelope"></i> blasquezdylan0@gmail.com</li>
                </ul>
            </div>
            <div class="col-md-12 col-sm-12">
                <p class="text-center license">
                    BLASQUEZ Dylan - Portfolio
                </p>
            </div>
        </div>
    </div>
</footer>